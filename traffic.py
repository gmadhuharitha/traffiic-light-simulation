import random
import time
import threading
import pygame
import sys

# Default values of signal timers
defaultGreen = {0:10, 1:10, 2:10, 3:10}
defaultRed = 150
defaultYellow = 5

signals = []
noOfSignals = 4
currentGreen = 0   # Indicates which signal is green currently
nextGreen = (currentGreen+1)%noOfSignals    # Indicates which signal will turn green next
currentYellow = 0   # Indicates whether yellow signal is on or off 

speeds = {'car':1.25, 'bus':0.8, 'truck':0.8, 'bike':1.5}  # average speeds of vehicles

# Coordinates of vehicles' start
x = {'right':[0,0,0], 'down':[755,727,697], 'left':[1400,1400,1400], 'up':[602,627,657]}    
y = {'right':[304,326,354], 'down':[0,0,0], 'left':[440,408,378], 'up':[800,800,800]}

vehicles = {'right': {0:[], 1:[], 2:[], 'crossed':0}, 'down': {0:[], 1:[], 2:[], 'crossed':0}, 'left': {0:[], 1:[], 2:[], 'crossed':0}, 'up': {0:[], 1:[], 2:[], 'crossed':0}}
vehicleTypes = {0:'car', 1:'bus', 2:'truck', 3:'bike'}
directionNumbers = {0:'right', 1:'down', 2:'left', 3:'up'}

# Coordinates of signal image, timer, and vehicle count
signalCoods = [(530,184),(810,184),(810,481),(530,481)]
signalTimerCoods = [(540,160),(820,160),(820,460),(540,460)]

# Coordinates of stop lines
stopLines = {'right': 590, 'down': 290, 'left': 790, 'up': 465}
defaultStop = {'right': 580, 'down': 280, 'left': 800, 'up': 475}
# stops = {'right': [580,580,580], 'down': [280,280,280], 'left': [800,800,800], 'up': [475,475,475]}

# Gap between vehicles
stoppingGap = 15    # stopping gap
movingGap = 15   # moving gap
